# Migration Tools #

### What is this repository for? ###

This is a script for helping developers that want to migrate from YouTrack to BitBucket's issue tracker. I couldn't find any available tool for translating between the two, and this was only written to suit my own needs.

* BitBucket format: https://confluence.atlassian.com/bitbucket/issue-import-export-data-format-330796872.html
* YouTrack data can be retrieved using the REST API, using GET /rest/export/{project}/issues?{max}.

### How do I get set up? ###

Included is a single script that shouldn't need any special dependencies.

The script generates the contents of the json file that is used as the data(see https://confluence.atlassian.com/bitbucket/issue-import-export-data-format-330796872.html).

### Contribution guidelines ###

The code is in a very initial state, so feel free to modify it to be more general, clean up the code etc.

### Who do I talk to? ###

If you're having trouble using this script or want to contribute, you can always contact me at yiftach.beer@gmail.com