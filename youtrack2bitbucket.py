#! /usr/bin/env python

__author__ = 'Yiftach Beer'


import sys
import datetime
import json
from xml.dom import minidom

PATH = "C:\Users\Yiftach\Desktop\issues.xml"

ISSUE_MAP = {
    'Priority': 'priority',
    'Type': 'kind',
    'State': 'status',
    'summary': 'title',
    'description': 'content',
    'comments': 'comments',
    'numberInProject': 'id',
    'updated': 'content_updated_on',
    'updated': 'updated_on',
    'created': 'created_on',
    'reporterName': 'reporter'
}

COMMENT_MAP = {
    'text': 'content',
    'author': 'user',
    'created': 'created_on',
    'issue': 'issue'
}

PRIORITY_MAP = {
    'Critical': 'critical',
    'Normal': 'trivial',
    'Major': 'major',
    'Minor': 'minor'}

STATUS_MAP = {
    'Fixed': 'resolved',
    'Submitted': 'open',
    'To be discussed': 'on hold'
}

KIND_MAP = {
    'Bug': 'bug',
    'Feature': 'enhancement',
    'Task': 'task',
    'Cosmetics': 'enhancement'
}


def migrate(dict, map):
    new_dict = {}
    for old, new in map.iteritems():
        if dict.has_key(old):
            new_dict[new] = dict[old]
    return new_dict


def fix_field_values(issues):
    issues['priority'] = PRIORITY_MAP[issues['priority']]
    issues['status'] = STATUS_MAP[issues['status']]
    issues['kind'] = KIND_MAP[issues['kind']]

    issues['id'] = int(issues['id'])

    # fix dates
    for field in ['created_on', 'updated_on']:
        issues[field] = fix_date(issues[field])

    if not 'content' in issues:
        issues['content'] = 'No further details were given for this issue.'


def fix_date(epoch):
    return datetime.datetime.fromtimestamp(int(epoch) / 1000.).isoformat()


def add_fields(issue, issue_dict):
    for field in issue.getElementsByTagName('field'):
        if field.childNodes:
            issue_dict[field.attributes['name'].value] = field.childNodes[0].childNodes[0].nodeValue


def add_comments(issue, comments, issue_id):
    for comment in issue.getElementsByTagName('comment'):
        comment_dict = {}
        for k, v in comment.attributes.items():
            comment_dict[k] = v
        comment_dict['issue'] = int(issue_id)
        comment_dict = migrate(comment_dict, COMMENT_MAP)
        comment_dict['created_on'] = fix_date(comment_dict['created_on'])
        comment_dict['id'] = len(comments) + 1
        comments.append(comment_dict)


def create_dict(path):
    whole = {}

    issues = []
    comments = []
    for issue in minidom.parse(path).getElementsByTagName('issue'):
        issue_dict = {}

        add_fields(issue, issue_dict)
        add_comments(issue, comments, issue_dict['numberInProject'])

        issue_dict = migrate(issue_dict, ISSUE_MAP)
        fix_field_values(issue_dict)

        issues.append(issue_dict)

    whole['issues'] = issues
    whole['comments'] = comments
    whole['meta'] = {'default_kind': 'enhancement'}

    return whole


def main():
    if len(sys.argv) < 2:
        print 'Please supply path to input XML file'
        return

    path = sys.argv[1]
    result = create_json(path)

    print result


def create_json(path):
    return json.dumps(create_dict(path), indent=4, separators=(',', ': '))


if __name__ == '__main__':
    main()
